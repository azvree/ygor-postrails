Rails.application.routes.draw do
  get '/comments/:id/posts', to: 'comments#posts'
  get '/like/:id/posts', to: 'like#posts'
  resources :likes
  resources :comments
  resources :posts
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
